package monopoly;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.Test;

public class SpielerTest {

    @Test
    public void testRandomTest() {
        Spieler spieler = new Spieler("Test", new Random(5));
        spieler.wuerfeln();
        assertEquals(6, spieler.getWuerfel1());
        assertEquals(5, spieler.getWuerfel2());
        assertFalse(spieler.hatPasch(), "");
    }
    
    @Test
    public void testHatPasch() {
        Random random = new Random(5);
        Spieler spieler = new Spieler("Test", random);
        random.nextInt(6);
        random.nextInt(6);
        spieler.wuerfeln();
        assertEquals(3, spieler.getWuerfel1());
        assertEquals(3, spieler.getWuerfel2());
        assertTrue(spieler.hatPasch(), "");
    }

}
