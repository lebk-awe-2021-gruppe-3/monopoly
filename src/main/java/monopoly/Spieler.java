package monopoly;

import java.util.Random;

import monopoly.felder.Feld;

/**
 * Klassenrahmen: David Rotert
 */
public class Spieler {
    public String name;
    public boolean istImGefaengnis = false;
    public int rundenImGefaengnis = 0;
    public Feld feld;
    public Monopoly monopoly;
    
    private Random randomGenerator;
    private int euro = 1500;
    private int wuerfel1 = 0;
    private int wuerfel2 = 0;
    
    public Spieler(String name) {
        this.name = name;
        this.randomGenerator = new Random();
    }
    
    public Spieler(String name, Random random) {
        this.name = name;
        this.randomGenerator = random;
    }
    
    /**
     * Wuerfelt 2 mal und setzt die Felder <code>zahl1</code> und <code>zahl2</code>,
     * wenn beide Zahlen gleich sind (Pasch)
     */
    public void wuerfeln() {
        this.wuerfel1 = this.wuerfelnInternal();
        System.out.println(this.wuerfel1);
        this.wuerfel2 = this.wuerfelnInternal();
        System.out.println(this.wuerfel2);
    }
    
    public int getAugenzahlGesamt() {
        return this.wuerfel1 + this.wuerfel2;
    }
    
    public boolean hatPasch() {
        if (this.wuerfel1 == this.wuerfel2) {
            return true;
        } else {
            return false;
        }
    }
    
    public int getWuerfel1() {
        return this.wuerfel1;
    }
    
    public int getWuerfel2() {
        return this.wuerfel2;
    }
    
    private int wuerfelnInternal() {
        Random random = this.randomGenerator;
        return random.nextInt(6) + 1;
    }
    
    public void hausBauen(String farbe) {
        
    }
    
    public void hotelBauen(String farbe) {
        
    }
    
    public int getSpielgeld() {
        return this.euro;
    }
    
    /**
     * Geld ein oder auszahlen
     * @param betrag
     * @return konnte bezahlen
     */
    public boolean geldEinAuszahlen(int betrag) {
        if (betrag < 0) {
            if (this.euro < betrag) {
                this.callBankrottEvent();
                return false;
            } else if (this.euro == betrag) {
                this.monopoly.gui.showOutputDialog("Bankrott", "Schade, du hast jetzt leider kein Geld mehr");
            }
        }
        this.euro += betrag;
        this.monopoly.gui.updatePlayerList();
        return true;
    }
    
    private void callBankrottEvent() {
        this.monopoly.gui.showOutputDialog("Bankrott", "Schade, du hast leider kein Geld mehr, um zu bezahlen");
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Spieler))
            return false;
        Spieler other = (Spieler) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
