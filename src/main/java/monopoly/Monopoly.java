package monopoly;

/**
 * Klassenrahmen: David Rotert
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import monopoly.felder.Feld;
import monopoly.felder.GefaengnisBesuch;
import monopoly.karten.Ereigniskarte;
import monopoly.karten.Gemeinschaftskarte;

public class Monopoly {
    public List<Spieler> spieler = new ArrayList<>();
    public List<Gemeinschaftskarte> gemeinschaftskarten = new ArrayList<>();
    public List<Ereigniskarte> ereigniskarten = new ArrayList<>();
    public Spieler aktuellerSpieler = null;
    public Brett spielBrett = null;
    public GUI gui;

    private int spielerAmZug;

    public Monopoly() {
        spielbrettErstellen();
        gui = new GUI(this);
    }

    private void spielbrettErstellen() {
        this.spielBrett = new Brett(this);
        spielBrett.felderInitialisieren();
    }

    private boolean spielerHinzufuegen(String name) {
        Spieler spieler = new Spieler(name, new Random() {
            @Override
            public int nextInt() {
                return 4;
            }
            
            @Override
            public int nextInt(int bound) {
                return 4;
            }
        });
        spieler.feld = this.spielBrett.felder[0];
        spieler.monopoly = this;
        if (!this.spieler.contains(spieler)) {
            this.spieler.add(spieler);
            this.gui.addPlayerToField(spieler.feld, spieler);
            this.gui.updatePlayerList();
            return true;
        } else {
            return false;
        }
    }

    public void spielStarten() {
        spielbrettErstellen();
        KartenErstellen();

        // Spieler hinzufügen
        while (true) {
            String result = this.gui.showInputDialog("Einen Spieler hinzufügen",
                    "Gegen Sie den Namen des Spielers ein:");
            if (result == null && this.spieler.size() >= 2) {
                break;
            } else if (result == null && this.spieler.size() < 2) {
                System.exit(0);
            } else if (!result.equals("")) {
                this.spielerHinzufuegen(result);
            }
        }

        int spielerAmZug = 0;
        aktuellerSpieler = spieler.get(spielerAmZug);
    }

    public void zugAusfuehren() {
        this.gui.showOutputDialog("", String.format("Du bist dran, " + this.aktuellerSpieler.name));
        spielzugWuerfelnUndSetzenFuerSpieler(this.aktuellerSpieler);
        // Nächster Spieler
        this.spielerAmZug = (this.spielerAmZug + 1) % this.spieler.size();
        aktuellerSpieler = spieler.get(this.spielerAmZug);
    }

    public void KartenErstellen() {
        // Gemeinschaftskarten
        gemeinschaftskarten
                .add(new Gemeinschaftskarte(this, 1, "Es ist dein Geburtstag. Ziehe von jedem Spieler 200 Euro ein."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 2, "Aus Lagerverkäufen erhältst Du 100 Euro"));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 3, "Du erbst 200 Euro."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 4, "Einkommenssteuerrückzahlung. Zahle 40 Euro."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 5,
                "Du hast den zweiten Preis in einer Schönheitswettbewerb gewonnen. Ziehe 200 Euro ein."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 6, "Rücke vor bis auf Los. "));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 7, "Bank-Irrtum zu deinen Gunsten. Ziehe 400 Euro ein."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 8, "Die Jahresrente wird fällig. Ziehe 200 Euro ein."));
        gemeinschaftskarten
                .add(new Gemeinschaftskarte(this, 9, "Du erhältst auf Vorzugs-Aktien 7% Dividende. 50 Euro."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 10, "Gehe zurück zu Badstraße."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 11,
                "Gehe in das Gefängnis. Begib Dich direkt dorthin. Gehe nicht über Los. Ziehe nicht 200 Euro ein."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 12, "Zahle deine Krankenhausrechnung. 200 Euro."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 13, "Zahle deine Versicherungssumme. 100 Euro."));
        gemeinschaftskarten.add(new Gemeinschaftskarte(this, 14, "Arzt-Kosten. Zahle 50 Euro."));

        // Ereigniskarten
        ereigniskarten.add(new Ereigniskarte(this, 1,
                "Rücke vor bis zur Seestraße. Wenn du über Los kommst, ziehe 200 Euro ein."));
        ereigniskarten.add(
                new Ereigniskarte(this, 2, "Du hast in einem Kreuzworträtselwettbewerb gewonnen. Ziehe 200 Euro ein."));
        ereigniskarten
                .add(new Ereigniskarte(this, 3, "Miete und Anleihezinsen werden fällig. Die Bank zahlt Dir 300 Euro."));
        ereigniskarten.add(new Ereigniskarte(this, 4, "Rücke bis auf Los vor."));
        ereigniskarten.add(new Ereigniskarte(this, 5, "Die Bank zahlt dir eine Dividende von 100 Euro."));
        ereigniskarten.add(new Ereigniskarte(this, 6, "Rücke vor bis zu Schlossallee."));
        ereigniskarten.add(new Ereigniskarte(this, 7,
                "Rücke vor bis zum Opernplatz. Wenn Du über Los kommst, ziehe 200 Euro ein."));
        ereigniskarten.add(new Ereigniskarte(this, 8, "Gehe 3 Felder zurück."));
        ereigniskarten.add(new Ereigniskarte(this, 9,
                "Lasse alle Deine Häuser renovieren. Zahle an die Bank für jedes Haus 50 Euro, für jedes Hotel 200 Euro."));
        ereigniskarten.add(new Ereigniskarte(this, 10,
                "Du wirst zu Straßenausbesserungsarbeiten herangezogen. Zahle für deine Häuser und Hotels. 50 Euro je Haus. 200 Euro je Hotel an die Bank."));
        ereigniskarten.add(new Ereigniskarte(this, 11, "Betrunken im Dienst. Strafe 400 Euro."));
        ereigniskarten.add(new Ereigniskarte(this, 12, "Strafe für zu schnelles Fahren 300 Euro."));
        ereigniskarten.add(new Ereigniskarte(this, 13,
                "Gehe in das Gefängnis. Begib Dich direkt dorthin. Gehe nicht über Los. Ziehe nicht 200 Euro ein."));
        ereigniskarten.add(new Ereigniskarte(this, 14, "Zahle Schulgeld. 100 Euro"));
    }

    /**
     * Würfelt für Spieler und führt einen Zug aus inklusive Events
     * 
     * @param spieler
     */
    private void spielzugWuerfelnUndSetzenFuerSpieler(Spieler spieler) {
        for (int paschCount = 0; paschCount < 3; paschCount++) {
            if (paschCount == 3) {
                this.gui.showOutputDialog("Gefängnis",
                        String.format("3 Mal Pasch. Du musst ins Gefängnis, %s!", spieler.name));
                this.setzeSpielerInGefaengnis(spieler);
                // Zug beenden, Spieler ist ja im Gefängnis
                break;
            }

            spieler.wuerfeln();
            int augenzahlSumme = spieler.getAugenzahlGesamt();
            this.gui.showOutputDialog(String.format("Spielzug für: %s", spieler.name),
                    String.format("Du hast %d & %d gewürfelt und darfst %d Felder weiter", spieler.getWuerfel1(),
                            spieler.getWuerfel2(), augenzahlSumme));

            this.setzeSpielerNFelderWeiter(spieler, augenzahlSumme);
            if (!spieler.hatPasch()) {
                break;
            } else {
                this.gui.showOutputDialog("Pasch",
                        String.format("Du hattest einen Pasch und bist noch einmal dran, %s", spieler.name));
            }
            ;
        }

    }

    public void setzeSpielerInGefaengnis(Spieler spieler) {
        for (Feld feld : this.spielBrett.felder) {
            if (feld instanceof GefaengnisBesuch) {
                this.setzeSpielerAufFeld(spieler, feld, true);
            }
        }
    }

    /**
     * Setzt Spieler weiter und führt die entsprechenden Events aus (über Los
     * gekommen, Event für das Feld, auf dem man landet)
     * 
     * @param spieler
     * @param n
     */
    public void setzeSpielerNFelderWeiter(Spieler spieler, int n) {
        int currentFeldIndex = this.getFeldIndexFuerSpieler(spieler);
        int nextFeldIndex = currentFeldIndex + n;
        // Modulo, da bei felder.length zurück gesetzt werden muss auf 0 (Startfeld)
        this.setzeSpielerAufFeld(spieler, this.spielBrett.felder[nextFeldIndex % this.spielBrett.felder.length]);
    }

    public void setzeSpielerAufFeld(Spieler spieler, Feld feld) {
        this.setzeSpielerAufFeld(spieler, feld, false);
    }

    public void setzeSpielerAufFeld(Spieler spieler, Feld feld, boolean direkt) {
        this.gui.removePlayerFromField(spieler.feld, spieler);
        this.gui.addPlayerToField(feld, spieler);
        if (spieler.feld.feldId > feld.feldId && !direkt) {
            this.eventSpielerUeberLos(spieler);
        }
        spieler.feld = feld;
        this.callFeldEventFuerSpieler(spieler);
    }

    /**
     * Index des Feldes, auf dem der Spieler grade steht
     * 
     * @param spieler
     * @return
     */
    public int getFeldIndexFuerSpieler(Spieler spieler) {
        return spieler.feld.feldId;
    }

    /**
     * Spieler kommt über Los und erhält Geld
     * 
     * @param spieler
     */
    private void eventSpielerUeberLos(Spieler spieler) {
        spieler.geldEinAuszahlen(200);
        this.gui.showOutputDialog("Über Los gekommen", String
                .format("Du bist über Los gekommen, %s. Du kassierst 200 Euro.", spieler.name, spieler.getSpielgeld()));
    }

    /**
     * Hier werden die Ereignisse aufgerufen, die passieren, wenn Spieler auf ein
     * Feld kommt. Startfeld auslassen, da diese Ereignis vor einem Zug ausgeführt
     * wird (passiert ja auch, wenn Spieler nicht auf dem Feld stehen bleibt)
     * 
     * @param spieler
     */
    private void callFeldEventFuerSpieler(Spieler spieler) {
        spieler.feld.aktionAusfuehren();
    }
}
