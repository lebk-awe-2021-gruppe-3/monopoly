package monopoly;

import monopoly.felder.Feld;
import monopoly.felder.GemeinschaftkarteFeld;
import monopoly.felder.Start;
import monopoly.felder.Freiparken;
import monopoly.felder.Gefaengnis;
import monopoly.felder.GefaengnisBesuch;
import monopoly.felder.EreigniskarteFeld;
import monopoly.felder.Steuer;
import monopoly.felder.Strasse;

/**
 * Klassenrahmen: David Rotert
 */

public class Brett {
    public Feld[] felder = new Feld[40];
    private Monopoly monopoly;


    public Brett(Monopoly monopoly) {
        this.monopoly = monopoly;
    }
    
    public void felderInitialisieren() {
        felder[0] = new Start(this.monopoly,0);
        felder[1] = new Strasse(this.monopoly, "Badstraße", 60, Feld.LILA,1);
        felder[2] = new GemeinschaftkarteFeld(this.monopoly,2);
        felder[3] = new Strasse(this.monopoly, "Turmstraße", 60, Feld.LILA,3);
        felder[4] = new Steuer(this.monopoly, "Einkommensteuer", 200,4);
        felder[5] = new Strasse(this.monopoly, "Südbahnhof", 200, Feld.SCHWARZ,5);
        felder[6] = new Strasse(this.monopoly, "Chausseestraße", 100, Feld.HELLBLAU,6);
        felder[7] = new EreigniskarteFeld(this.monopoly,7);
        felder[8] = new Strasse(this.monopoly, "Elisenstraße", 100, Feld.HELLBLAU,8);
        felder[9] = new Strasse(this.monopoly, "Poststraße", 120, Feld.HELLBLAU,9);
        felder[10]= new GefaengnisBesuch(this.monopoly,10);
        felder[11]= new Strasse(this.monopoly, "Seestraße", 140, Feld.PINK,11);
        felder[12]= new Strasse(this.monopoly, "Elektrizitätswerk", 150, Feld.GRAU,12);
        felder[13]= new Strasse(this.monopoly, "Hafenstraße", 140, Feld.GRAU,13);
        felder[14]= new Strasse(this.monopoly, "Neue Straße", 160, Feld.PINK,14);
        felder[15]= new Strasse(this.monopoly, "Westbahnhof", 200, Feld.SCHWARZ,15);
        felder[16]= new Strasse(this.monopoly, "Münchner Straße", 180, Feld.ORANGE,16);
        felder[17]= new GemeinschaftkarteFeld(this.monopoly,17);
        felder[18]= new Strasse(this.monopoly, "Wiener Straße", 180, Feld.ORANGE,18);
        felder[19]= new Strasse(this.monopoly, "Berliner Straße", 200, Feld.ORANGE,19);
        felder[20]= new Freiparken(this.monopoly,20);
        felder[21]= new Strasse(this.monopoly, "Theaterstraße", 220, Feld.ROT,21);
        felder[22]= new EreigniskarteFeld(this.monopoly,22);
        felder[23]= new Strasse(this.monopoly, "Museumsstraße", 220, Feld.ROT,23);
        felder[24]= new Strasse(this.monopoly, "Opernplatz", 230, Feld.ROT,24);
        felder[25]= new Strasse(this.monopoly, "Nordbahnhof", 200, Feld.SCHWARZ,25);
        felder[26]= new Strasse(this.monopoly, "Lessingstraße", 260, Feld.GELB,26);
        felder[27]= new Strasse(this.monopoly, "Schillerstraße", 260, Feld.GELB,27);
        felder[28]= new Strasse(this.monopoly, "Wasserwerk", 150, Feld.GRAU,28);
        felder[29]= new Strasse(this.monopoly, "Goethestraße", 280, Feld.GELB,29);
        felder[30]= new Gefaengnis(this.monopoly,30);
        felder[31]= new Strasse(this.monopoly, "Rathausplatz", 300, Feld.GRUEN,31);
        felder[32]= new Strasse(this.monopoly, "Hauptstraße", 300, Feld.GRUEN,32);
        felder[33]= new GemeinschaftkarteFeld(this.monopoly,33);
        felder[34]= new Strasse(this.monopoly, "Bahnhofsstraße", 320, Feld.GRUEN,34);
        felder[35]= new Strasse(this.monopoly, "Ostbahnhof", 200, Feld.SCHWARZ,35);
        felder[36]= new EreigniskarteFeld(this.monopoly,36);
        felder[37]= new Strasse(this.monopoly, "Parkstraße", 350, Feld.DUNKELBLAU,37);
        felder[38]= new Steuer(this.monopoly, "Zusatzsteuer", 150,38);
        felder[39]= new Strasse(this.monopoly, "Schlossallee", 400, Feld.DUNKELBLAU,39);
    }
}
