package monopoly.karten;

import monopoly.Monopoly;

/**
 * Klassenrahmen: David Rotert
 */

public class Gemeinschaftskarte {
    public int id;
    public String beschreibung;
    private Monopoly monopoly;

    public Gemeinschaftskarte(Monopoly monopoly, int pId, String pBeschreibung) {
        this.monopoly = monopoly;
        this.id = pId;
        this.beschreibung = pBeschreibung;
    }

    /**
     * Karte ist gezogen, Die Aktion wird ausgeführt
     */
    public void karteZiehen() {
        switch (this.id) {
            case 1: //"Es ist dein Geburtstag. Ziehe von jedem Spieler 200 Euro ein."
            case 2:
                ausfuehren(100, this.beschreibung);
                break;
            case 3:
                ausfuehren(200, this.beschreibung);
                break;
            case 4:
                ausfuehren(-40, this.beschreibung);
                break;
            case 5:
                ausfuehren(200, this.beschreibung);
                break;
            case 6:
                ausfuehrenFeld(0, this.beschreibung);
                break;
            case 7:
                ausfuehren(400, this.beschreibung);
                break;
            case 8:
                ausfuehren(200, this.beschreibung);
                break;
            case 9:
                ausfuehren(50, this.beschreibung);
                break;
            case 10:
                ausfuehrenFeld(1, this.beschreibung);
                break;
            case 11:
                ausfuehrenGeheInsGefaengnis(10, this.beschreibung);
                break;
            case 12:
                ausfuehren(-200, this.beschreibung);
                break;
            case 13:
                ausfuehren(-100, this.beschreibung);
                break;
            case 14:
                ausfuehren(-50, this.beschreibung);
                break;
        }
    }

    /**
     * Führt eine Aktion basierend auf Geldtransaktionen aus
     *
     * @param betrag
     * @param beschreibungKarte
     */
    public void ausfuehren(int betrag, String beschreibungKarte) {
        this.monopoly.gui.showOutputDialog("Gemeinschaftskarte", beschreibungKarte);
        this.monopoly.aktuellerSpieler.geldEinAuszahlen(betrag);
    }

    private void ausfuehrenFeld(int feld, String beschreibungKarte) {
        this.monopoly.gui.showOutputDialog("Gemeinschaftskarte", beschreibungKarte);
        this.monopoly.setzeSpielerAufFeld(this.monopoly.aktuellerSpieler, this.monopoly.spielBrett.felder[feld]);
    }

    private void ausfuehrenGeheInsGefaengnis(int feld, String beschreibungKarte) {
        this.monopoly.gui.showOutputDialog("Gemeinschaftskarte", beschreibungKarte);
        this.monopoly.setzeSpielerInGefaengnis(this.monopoly.aktuellerSpieler);
    }

    private void ausfuehrenGeburtstagGeschenk(int betrag, String beschreibungKarte) {
        this.monopoly.gui.showOutputDialog("Gemeinschaftskarte", beschreibungKarte);
        this.monopoly.aktuellerSpieler.geldEinAuszahlen(betrag * monopoly.spieler.size());
        for (int i = 0; i < monopoly.spieler.size(); i++) {
            monopoly.spieler.get(i).geldEinAuszahlen(-betrag);
        }
    }
}
