package monopoly.karten;

import monopoly.Monopoly;
import monopoly.Spieler;

/**
 * Klassenrahmen: David Rotert
 */

public class Ereigniskarte {
    public int id;
    public String beschreibung;
    private Monopoly monopoly;
    
    public Ereigniskarte(Monopoly monopoly, int pId, String pBeschreibung)
    {
        id = pId;
        beschreibung = pBeschreibung;
        this.monopoly= monopoly;
    }

    /**
     * Karte ist gezogen, Die Aktion wird ausgeführt
     */
    public void karteZiehen()
    {
        switch(this.id)
        {
            case 1: ausfuehrenFeld(11,this.beschreibung); break;
            case 2: ausfuehren(200,this.beschreibung); break;
            case 3: ausfuehren(300,this.beschreibung); break;
            case 4: ausfuehrenFeld(0,this.beschreibung); break;
            case 5: ausfuehren(100,this.beschreibung); break;
            case 6: ausfuehrenFeld(39,this.beschreibung); break;
            case 7: ausfuehren(200,this.beschreibung); break;
            case 8: ausfuehrenFeld(24,this.beschreibung); break;
            case 9:  //Hausrenovierung
            case 10: //Hausrenovierung
            case 11: ausfuehren(-400,this.beschreibung); break;
            case 12: ausfuehren(-300,this.beschreibung); break;
            case 13: ausfuehrenGeheInsGefaengnis(10,this.beschreibung); break;
            case 14: ausfuehren( -100,this.beschreibung); break;

        }
    }

    /**
     * Führt eine Aktion basierend auf Geldtransaktionen aus
     * @param betrag
     * @param beschreibung
     */
    private void ausfuehren(int betrag, String beschreibung)
    {
        this.monopoly.gui.showOutputDialog("Ereigniskarte", beschreibung);
        this.monopoly.aktuellerSpieler.geldEinAuszahlen(betrag);
        
    }
    
    /**
     * Führt Aktionen für Felder (gehe ins Gefängnis etc.) aus
     * @param feld
     * @param beschreibungKarte
     */
    private void ausfuehrenFeld(int feld, String beschreibungKarte)
    {
        this.monopoly.gui.showOutputDialog("Ereigniskarte", beschreibung);
        this.monopoly.setzeSpielerAufFeld(this.monopoly.aktuellerSpieler, this.monopoly.spielBrett.felder[feld]);
    }

    private void ausfuehrenGeheInsGefaengnis(int feld,String beschreibungKarte){
        this.monopoly.gui.showOutputDialog("Ereigniskarte", beschreibung);
        this.monopoly.setzeSpielerInGefaengnis(this.monopoly.aktuellerSpieler);
    }
    
}
