package monopoly.felder;
import monopoly.Monopoly;

public class Gefaengnis extends Feld {
    public Monopoly monopoly;

    public Gefaengnis(Monopoly pMonopoly, int pFeldId)
    {
        super(pFeldId, "Geh ins Gefängnis", WEISS);
        this.monopoly = pMonopoly;
        this.feldId = pFeldId;
    }

    @Override
    public void aktionAusfuehren() {
        this.monopoly.gui.showOutputDialog("Gehe ins Gefängnis", String.format("Gehe ins Gefängnis, %s.", this.monopoly.aktuellerSpieler.name));
        this.monopoly.setzeSpielerInGefaengnis(this.monopoly.aktuellerSpieler);
    }
}
