package monopoly.felder;
import monopoly.Monopoly;
import monopoly.karten.Ereigniskarte;

import java.util.Random;

public class EreigniskarteFeld extends Feld {
    public Monopoly monopoly;

    public EreigniskarteFeld(Monopoly pMonopoly,int pFeldID)
    {
        super(pFeldID, "Ereigniskarte", WEISS);
        this.monopoly = pMonopoly;
        this.feldId = pFeldID;

    }

    @Override
    public void aktionAusfuehren()
    {
        int anzahl = this.monopoly.ereigniskarten.size();

        Random zufall = new Random();
        int zufallsZahl = zufall.nextInt(anzahl);
        
        Ereigniskarte karte = this.monopoly.ereigniskarten.get(zufallsZahl);
        karte.karteZiehen();
    }

}
