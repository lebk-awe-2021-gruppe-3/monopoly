package monopoly.felder;

import monopoly.Monopoly;

public class Steuer extends Feld {
    public int preis;
    public Monopoly monopoly;

    public Steuer(Monopoly pMonopoly, String name, int preis, int pFeldId) {
        super(pFeldId, name, WEISS);
        this.monopoly = pMonopoly;
        this.name = name;
        this.preis = preis;
        this.feldId = pFeldId;
    }

    @Override
    public void aktionAusfuehren() {
        this.monopoly.gui.showOutputDialog("Steuer",
                String.format("Du musst %s Spielgeld %s zahlen, %s", preis, name, 
                        monopoly.aktuellerSpieler.name));
        this.monopoly.aktuellerSpieler.geldEinAuszahlen(-preis);
    }
}
