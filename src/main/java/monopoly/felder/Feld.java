package monopoly.felder;

public abstract class Feld {
    public final static String WEISS = "Weiß";
    public final static String SCHWARZ = "Schwarz";
    public final static String LILA = "Lila";
    public final static String HELLBLAU = "Hellblau";
    public final static String PINK = "Pink";
    public final static String ORANGE = "Orange";
    public final static String ROT = "Rot";
    public final static String GELB = "Gelb";
    public final static String GRUEN = "Grün";
    public final static String HELLGRUEN = "Hellgrün";
    public final static String DUNKELBLAU = "Dunkelblau";
    public final static String GRAU = "Grau";
    public final static String HELLGRAU = "Hellgrau";

    public String name;
    public String farbe;
    public int feldId;
    
    public Feld(int id, String name, String farbe) {
        this.feldId = id;
        this.name = name;
        this.farbe = farbe;
    }

    public abstract void aktionAusfuehren();
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Feld)) {
            return false;
        } else {
            return ((Feld) obj).name.equals(this.name) && ((Feld) obj).farbe.equals(this.farbe);
        }
    }

}
