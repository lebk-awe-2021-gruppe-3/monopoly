package monopoly.felder;
import monopoly.Monopoly;
import monopoly.karten.Gemeinschaftskarte;

import java.util.Random;

public class GemeinschaftkarteFeld extends Feld {
    private Monopoly monopoly;

    public GemeinschaftkarteFeld(Monopoly monopoly, int pFeldId) {
        super(pFeldId, "Gemeinschaftkarte", WEISS);
        this.monopoly = monopoly;
        this.feldId = pFeldId;
    }

    @Override
    public void aktionAusfuehren() {
        int anzahl = this.monopoly.gemeinschaftskarten.size();

        Random zufall = new Random(); 
        int zufallsZahl = zufall.nextInt(anzahl);
        
        Gemeinschaftskarte karte = this.monopoly.gemeinschaftskarten.get(zufallsZahl);
        karte.karteZiehen();
    }
}
