package monopoly.felder;
import monopoly.Monopoly;

public class Start extends Feld {
    public Monopoly monopoly;

    public Start(Monopoly pMonopoly, int pFeldId)
    {
        super(pFeldId, "Start", HELLGRAU);
        this.monopoly = pMonopoly;
        this.feldId = pFeldId;
    }

    /**
     * Keine Aktion! Das Event "über Los gekommen" wird beim Setzen des Spielers ausgeführt,
     * nicht hier.
     */
    @Override
    public void aktionAusfuehren() {
    }
}
