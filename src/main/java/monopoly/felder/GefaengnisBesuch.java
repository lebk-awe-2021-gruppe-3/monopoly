package monopoly.felder;
import monopoly.Monopoly;

public class GefaengnisBesuch extends Feld {
    public Monopoly monopoly;

    public GefaengnisBesuch(Monopoly pMonopoly, int pFeldId)
    {
        super(pFeldId, "Zu Besuch im Gefängnis", WEISS);
        this.monopoly = pMonopoly;
        this.feldId = pFeldId;
    }
    
    @Override
    public void aktionAusfuehren() {
        gefaengnisController();
    }

    public boolean ausbruchDurchWuerfeln(){
        monopoly.aktuellerSpieler.wuerfeln();
        return monopoly.aktuellerSpieler.hatPasch();
    }

    public boolean ausbruchDurchBezahlen(){
        monopoly.aktuellerSpieler.geldEinAuszahlen(-50);
        return true;
    }

    public void gefaengnisController(){
        int rundenImGeFaengnis = monopoly.aktuellerSpieler.rundenImGefaengnis;
        if(rundenImGeFaengnis < 3){
            boolean istAusgebrochen = ausbruchDurchWuerfeln();
            monopoly.aktuellerSpieler.istImGefaengnis = !istAusgebrochen;
            if(!istAusgebrochen) monopoly.aktuellerSpieler.rundenImGefaengnis +=1;
            else monopoly.aktuellerSpieler.rundenImGefaengnis =0;
        }
        else{
            monopoly.aktuellerSpieler.istImGefaengnis = !ausbruchDurchBezahlen();
            monopoly.aktuellerSpieler.rundenImGefaengnis =0;
        }
    }
}
