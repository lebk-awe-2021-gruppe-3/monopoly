package monopoly.felder;
import monopoly.Monopoly;
import monopoly.Spieler;
import monopoly.Brett;

public class Strasse extends Feld {
    public Spieler besitzer;
    public int preis;
    public Monopoly monopoly;

    public Strasse(Monopoly pMonopoly, String pName, int pPreis, String pFarbe,int pFeldId) {
        super(pFeldId, pName, pFarbe);
        this.monopoly = pMonopoly;
        this.preis = pPreis;
        this.feldId = pFeldId;

    }

    @Override
    public void aktionAusfuehren() {
        if (besitzer == null) {
            feldkaufen();
        } else if (!besitzer.name.equals(monopoly.aktuellerSpieler.name)) {
            mieteberechnen();
        }
    }

    private void mieteberechnen(){
        int miete;
        switch(farbe){
            case "grau":
                miete = mieteVonVersorgungswerk();
                break;
            case "Schwarz":
                miete = mieteVonBanhoefen();
                break;
            default:
                miete = mieteVonregulaerenStrassen();
                break;
        }
        monopoly.aktuellerSpieler.geldEinAuszahlen(-miete);
        besitzer.geldEinAuszahlen(miete);
    }

    private int mieteVonVersorgungswerk(){
        int graueFelder =0;
        int graueFelderImBesitz=0;
        int i=0;
        while (graueFelder <2){
            Strasse feld = (Strasse) this.monopoly.spielBrett.felder[i];
            if (feld.farbe.equals("grau")){
                graueFelder+=1;
                if(feld.besitzer.equals(this.besitzer)){
                    graueFelderImBesitz +=1;
                }
            }
            i++;
        }

        if (graueFelderImBesitz==1)return monopoly.aktuellerSpieler.getAugenzahlGesamt() * 4;
        if (graueFelderImBesitz==2)return monopoly.aktuellerSpieler.getAugenzahlGesamt() * 10;
    return 0;
    }

    private int mieteVonBanhoefen(){
        int schwarzeFelder =0;
        int schwarzeFelderImBesitz=0;
        int i=0;
        while (schwarzeFelder <4){
            Strasse feld = (Strasse) this.monopoly.spielBrett.felder[i];
            if (feld.farbe.equals("schwarz")){
                schwarzeFelder+=1;
                if(feld.besitzer.equals(this.besitzer)){
                    schwarzeFelderImBesitz +=1;
                }
            }
            i++;
        }
        return 50*schwarzeFelderImBesitz;
    }

    private int mieteVonregulaerenStrassen(){
        int strasseFelder =0;
        int strasseFelderImBesitz=0;
        int gesamtFelderDerStrasse=0;
        for(int j=0;j<monopoly.spielBrett.felder.length;j++){
            if (monopoly.spielBrett.felder[j].farbe.equals(this.farbe)) gesamtFelderDerStrasse+=1;
        }
        int i=0;
        while (strasseFelder < gesamtFelderDerStrasse){
            Strasse feld = (Strasse) this.monopoly.spielBrett.felder[i];
            if (feld.farbe.equals(this.farbe)){
                strasseFelder+=1;
                if(feld.besitzer.equals(this.besitzer)){
                    strasseFelderImBesitz +=1;
                }
            }
            i++;
        }
        if (strasseFelderImBesitz==gesamtFelderDerStrasse){
            //hier abfragen ob häuser gebaut wurden

            return this.preis/2;
        }
        else return this.preis/10;
    }

    private void feldkaufen(){
        boolean feldKaufen = monopoly.gui.showYesNoDialog("Feld kaufen", String.format("Möchtest du das Feld \"%s\" für %d Euro Kaufen, %s?", this.name, this.preis, monopoly.aktuellerSpieler.name));
        if (feldKaufen) {
            monopoly.aktuellerSpieler.geldEinAuszahlen(-preis);
            besitzer = monopoly.aktuellerSpieler;
        }
    }
}
