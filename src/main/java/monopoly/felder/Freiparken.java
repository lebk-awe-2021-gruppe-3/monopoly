package monopoly.felder;
import monopoly.Monopoly;

public class Freiparken extends Feld {
    public Monopoly monopoly;

    public Freiparken(Monopoly pMonopoly,int pFeldId)
    {
        super(pFeldId, "Freiparken", WEISS);
        this.monopoly = pMonopoly;
        this.feldId = pFeldId;
    }

    /**
     * Keine Aktion!
     */
    @Override
    public void aktionAusfuehren() {
    }
}
