package monopoly;

import java.awt.Color;

public class Farben {
    public final static Color WEISS = new Color(255, 255, 255);
    public final static Color SCHWARZ = new Color(0, 0, 0);
    public final static Color LILA = new Color(128, 0, 255);
    public final static Color HELLBLAU = new Color(0, 183, 255);
    public final static Color PINK = new Color(255, 133, 233);
    public final static Color ORANGE = new Color(255, 155, 0);
    public final static Color ROT = new Color(255, 0, 14);
    public final static Color GELB = new Color(255, 240, 0);
    public final static Color GRUEN = new Color(0, 90, 11);
    public final static Color HELLGRUEN = new Color(0, 212, 28);
    public final static Color DUNKELBLAU = new Color(0, 20, 255);
    public final static Color GRAU = new Color(103, 103, 103);
    public final static Color HELLGRAU = new Color(240, 240, 240);
}
