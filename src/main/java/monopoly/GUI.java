package monopoly;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import monopoly.felder.Feld;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class GUI extends JFrame {
    /**
     * Berechnet aus dem GridLayout index den Array Index für die Felder.
     * 
     * @param i Index. In Schleifen, bitte ab 1 beginnen!
     * @return Feld Array Index. <code>null</code>, wenn für Index kein Feld
     *         gezeichnet werden soll.
     */
    private static Integer gridLayoutIndexToFieldIndex(int i) {
        if (i <= 11) {
            return i - 1;
        } else if (i % 11 == 0) {
            return 9 + (i / 11);
        } else if (i >= 112) {
            return 30 - ((i % 11) - 1);
        } else if (i % 11 == 1) {
            return 40 - ((i - 1) / 11);
        } else {
            return null;
        }
    }
    
    private Monopoly monopoly;
    
    private JLabel playersLabel;
    
    private java.util.List<JLabel> fieldLabels = Arrays.asList(new JLabel[40]);

    /**
     * Konstruktor
     *
     * @param monopoly
     */
    public GUI(Monopoly monopoly) {
        this.monopoly = monopoly;

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Monopoly");
        this.setSize(1000, 800);
        this.setLayout(new BorderLayout(0, 0));

        // Panels hinzufügen
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.PAGE_AXIS));
        JButton wuerfeln = new JButton("Würfeln");
        wuerfeln.addActionListener(event -> {
            this.monopoly.zugAusfuehren();
        });
        leftPanel.add(wuerfeln);
        this.playersLabel = new JLabel();
        leftPanel.add(playersLabel);
        this.add(leftPanel, BorderLayout.LINE_START);
        
        // Spielbrett hinzufügen
        this.add(this.spielbrettPanel(), BorderLayout.CENTER);

        this.setVisible(true);
    }

    /**
     * Spielbrett Panel
     *
     * @return Panel
     */
    private JPanel spielbrettPanel() {
        JPanel spielbrettPanel = new JPanel();
        spielbrettPanel.setPreferredSize(new Dimension(800, 800));
        spielbrettPanel.setBackground(new Color(255, 255, 255));

        // Berechne die Länge Anzahl der Felder auf jeder Seite dynamisch (= 11)
        int layoutSize = ((this.monopoly.spielBrett.felder.length / 2) + 2) / 2;
        
        // Benutze Grid, um Monopoly Spielbrett darzustellen
        GridLayout brettLayout = new GridLayout(layoutSize, layoutSize);
        spielbrettPanel.setLayout(brettLayout);
        for (int i = 1; i <= layoutSize * layoutSize; i++) {
            Integer index = gridLayoutIndexToFieldIndex(i);
            if (index == null) {
                spielbrettPanel.add(new JPanel());
                continue;
            }
            Feld feld = this.monopoly.spielBrett.felder[index];

            JLabel label = new JLabel("<html>" + feld.name + "<br>");
            label.setOpaque(true);
            Color background = Farben.WEISS;
            Color foreground = Farben.SCHWARZ;
            switch (feld.farbe) {
            case Feld.WEISS:
                background = Farben.WEISS;
                foreground = Farben.SCHWARZ;
                break;
            case Feld.SCHWARZ:
                background = Farben.SCHWARZ;
                foreground = Farben.WEISS;
                break;
            case Feld.ROT:
                background = Farben.ROT;
                foreground = Farben.WEISS;
                break;
            case Feld.GRUEN:
                background = Farben.GRUEN;
                foreground = Farben.WEISS;
                break;
            case Feld.HELLGRUEN:
                background = Farben.HELLGRUEN;
                foreground = Farben.SCHWARZ;
                break;
            case Feld.GELB:
                background = Farben.GELB;
                break;
            case Feld.DUNKELBLAU:
                background = Farben.DUNKELBLAU;
                foreground = Farben.WEISS;
                break;
            case Feld.HELLBLAU:
                background = Farben.HELLBLAU;
                break;
            case Feld.LILA:
                background = Farben.LILA;
                foreground = Farben.WEISS;
                break;
            case Feld.PINK:
                background = Farben.PINK;
                break;
            case Feld.ORANGE:
                background = Farben.ORANGE;
                break;
            case Feld.GRAU:
                background = Farben.GRAU;
                foreground = Farben.WEISS;
                break;
            case Feld.HELLGRAU:
                background = Farben.HELLGRAU;
                break;
            }
            label.setBackground(background);
            label.setForeground(foreground);
            label.setBorder(BorderFactory.createLineBorder(Farben.SCHWARZ, 1));
            Font labelFont = label.getFont();
            label.setFont(new Font(labelFont.getName(), labelFont.getStyle(), 11));
            this.fieldLabels.set(index, label);
            spielbrettPanel.add(label);
        }

        return spielbrettPanel;
    }
    
    public void removePlayerFromField(Feld f, Spieler s) {
        String oldText = this.fieldLabels.get(f.feldId).getText();
        this.fieldLabels.get(f.feldId).setText(oldText.replace(s.name + "<br>", ""));
    }
    
    public void addPlayerToField(Feld f, Spieler s) {
        String oldText = this.fieldLabels.get(f.feldId).getText();
        this.fieldLabels.get(f.feldId).setText(oldText + s.name + "<br>");
    }
    
    public void updatePlayerList() {
        String text = "<html>";
        for (Spieler spieler: this.monopoly.spieler) {
            text += String.format("%s: %d$<br>", spieler.name, spieler.getSpielgeld());
        }
        this.playersLabel.setText(text);
    }

    /**
     * Dialogfeld zum Anzeigen von Nachrichten
     *
     * @param header Überschrift
     * @param text   Text
     */
    public void showOutputDialog(String header, String text) {
        JOptionPane.showMessageDialog(this, text, header, JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Zeigt einen Eingabedialog an.
     * 
     * @param header Überschrift
     * @param text   Text
     * @return Der Text aus dem Input Feld. Wenn der Button "Abbrechen" gedrückt
     *         wird, wird <code>null</code> zurückgegeben.
     */
    public String showInputDialog(String header, String text) {
        String result = (String) JOptionPane.showInputDialog(this, text, header,
                JOptionPane.PLAIN_MESSAGE, null, null, "");
        return result;
    }
    
    public Boolean showYesNoDialog(String header, String text) {
        int n = JOptionPane.showConfirmDialog(
                this,
                text,
                header,
                JOptionPane.YES_NO_OPTION);
        if (n < 0 || n == 1) {
            return false;
        } else {
            return true;
        }
    }
}
